// import { combineReducers, createStore } from "redux";
import { legacy_createStore as createStore, applyMiddleware } from "redux";
import { combineReducers } from "redux-immutable";
import thunk from "redux-thunk";
import example from "../pages/example/store";

export default createStore(
  combineReducers({
    // login: reducer,
    // order: reducer,
    example,
  }),
  applyMiddleware(thunk)
);
