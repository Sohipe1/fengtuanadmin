import React, { useState } from "react";
import {
  UploadOutlined,
  HomeOutlined,
  UserOutlined,
  LineChartOutlined,
  SmileOutlined,
  IdcardOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
// 导入路由
import { Outlet, useNavigate } from "react-router-dom";
// 导入组件
import Top from "./components/top";
//导入样式
import { Container } from "./styled";
const { Sider, Content, Header } = Layout;
const App = () => {
  // 状态
  const [collapsed, setCollapsed] = useState(false);
  // 路由
  const navigator = useNavigate();
  return (
    <Container>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="inline"
            onClick={({ key, keyPath, domEvent }) => {
              // console.log(key, keyPath, domEvent);
              // console.log(keyPath.shift());
              let urls = {
                "1-0": "/admin",
                "1-1": "/admin/welcome",
                "1-2": "/admin/history",
                "2-1": "/admin/users",
                "2-2": "/admin/users/create",
              };
              let urlsKeys = keyPath.shift();
              navigator(urls[urlsKeys]);
            }}
            defaultSelectedKeys={["1"]}
            items={[
              {
                key: "1-0",
                icon: <HomeOutlined />,
                label: "后台首页",
                children: [
                  {
                    key: "1-1",
                    icon: <SmileOutlined />,
                    label: "欢迎页",
                  },
                  {
                    key: "1-2",
                    icon: <LineChartOutlined />,
                    label: "访客记录",
                  },
                ],
              },
              {
                key: "2-0",
                icon: <IdcardOutlined />,
                label: "用户管理",
                children: [
                  {
                    key: "2-1",
                    icon: <UserOutlined />,
                    label: "用户列表",
                  },
                  {
                    key: "2-2",
                    icon: <UserOutlined />,
                    label: "用户创建",
                  },
                ],
              },
              {
                key: "3-0",
                icon: <UploadOutlined />,
                label: "退出",
              },
            ]}
          />
        </Sider>
        <Layout className="site-layout">
          <Header>
            <Top collapsed={collapsed} setCollapsed={setCollapsed} />
          </Header>

          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
              overflowY: "scroll",
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
    </Container>
  );
};
export default App;
