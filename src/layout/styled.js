import styled from "styled-components";
export const Container = styled.div`
  height: 100%;
  .ant-layout.ant-layout-has-sider {
    height: 100%;
  }
  /* .ant-menu.ant-menu-dark {
    background-color: #2d3a4b;
  } */
  .ant-layout-header {
    background: #fff;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .anticon svg {
    display: inline-block;
    font-size: 22px;
  }
  .left {
    width: 70%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    .anticon svg {
      margin-right: 20px;
    }
  }
`;
