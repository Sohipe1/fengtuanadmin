// 导入API
import React from "react";
import { Breadcrumb } from "antd";
import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
function Top(props) {
  let { collapsed, setCollapsed } = props;
  return (
    <>
      <div className="left">
        {React.createElement(
          collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
          {
            className: "trigger",
            onClick: () => setCollapsed(!collapsed),
          }
        )}
        <Breadcrumb>
          <Breadcrumb.Item>后台首页</Breadcrumb.Item>
          <Breadcrumb.Item>用户管理</Breadcrumb.Item>
          <Breadcrumb.Item>用户列表</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="right">唔西迪西小推车 （超级管理员）</div>
    </>
  );
}
export default Top;
