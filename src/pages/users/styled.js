import styled from "styled-components";

export const Container = styled.div`
  .ant-table {
    margin: 20px 0;
  }

  .ant-pagination {
    width: 100%;
    text-align: right;
    .ant-pagination-jump-next
      .ant-pagination-item-container
      .ant-pagination-item-ellipsis,
    .ant-pagination-jump-prev
      .ant-pagination-item-container
      .ant-pagination-item-ellipsis {
      top: 10px;
    }
    .ant-pagination-jump-prev
      .ant-pagination-item-container
      .ant-pagination-item-link-icon,
    .ant-pagination-jump-next
      .ant-pagination-item-container
      .ant-pagination-item-link-icon {
      margin-top: 5px;
    }
    .ant-pagination-item-link {
      line-height: 1;
    }
  }
`;
