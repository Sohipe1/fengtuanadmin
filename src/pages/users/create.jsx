// 导入API
// 导入样式
import { Container } from "./create.styled";
// 导入路由
import { useNavigate } from "react-router-dom";
// 导入组件
import { Card, Form, Select, Input, Button, message } from "antd";
const { Option } = Select;

function Create() {
  // 路由
  const navigate = useNavigate();
  // 表单登录
  const onFinish = (values) => {
    console.log("Success:", values);
    message.success("创建成功");
    navigate("/admin/users");
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
    message.error("创建失败");
  };
  return (
    <Container>
      <Card
        title="用户创建"
        extra={
          <Button
            type="primary"
            onClick={() => {
              navigate("/admin/users");
            }}
          >
            返回
          </Button>
        }
      >
        <Form
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            name="question"
            validateTrigger="onBlur"
            rules={[
              {
                required: true,
                message: "请选择密保问题",
              },
            ]}
          >
            <Select placeholder="请选择密保问题">
              <Option value="你大爷的名字">你大爷的名字</Option>
              <Option value="你其中一位讲师的名字">你其中一位讲师的名字</Option>
              <Option value="你叔叔的名字">你叔叔的名字</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="answer"
            validateTrigger="onBlur"
            rules={[
              {
                required: true,
                message: "请输入密保答案",
              },
            ]}
          >
            <Input placeholder="请输入密保答案" />
          </Form.Item>
          <Form.Item
            name="uname"
            validateTrigger="onBlur"
            rules={[
              {
                validator: (_, value) => {
                  if (!value) return Promise.reject(new Error("请输入用户名"));

                  if (value.length < 2 || value.length > 10) {
                    return Promise.reject(new Error("用户名只能2~10个字符"));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="请输入用户名" />
          </Form.Item>
          <Form.Item
            name="password"
            validateTrigger="onBlur"
            rules={[
              {
                required: true,
                message: "请输入密码",
              },
            ]}
          >
            <Input placeholder="请输入密码" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              创建
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Container>
  );
}
export default Create;
