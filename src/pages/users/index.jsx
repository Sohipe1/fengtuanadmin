import { useState } from "react";
// 导入样式
import { Container } from "./styled";
// 导入API
// 导入路由
import { useNavigate } from "react-router-dom";
// 导入组件
import UserEdit from "./components/userEdit";
import {
  FormOutlined,
  DeleteOutlined,
  ExclamationCircleFilled,
} from "@ant-design/icons";
import {
  Card,
  Button,
  Input,
  DatePicker,
  Table,
  Pagination,
  Modal,
} from "antd";
const { Search } = Input;
const { RangePicker } = DatePicker;

const onChange = (value, dateString) => {
  console.log("Selected Time: ", value);
  console.log("Formatted Selected Time: ", dateString);
};
// 方法
const handleDel = () => {
  Modal.confirm({
    // title: "Are you sure delete this task?",
    title: "确定删除吗🐎?",
    icon: <ExclamationCircleFilled />,
    // content: "Some descriptions",
    content: "删除就没有啦啊/(ㄒoㄒ)/~~",
    okText: "Yes",
    okType: "danger",
    okButtonProps: {
      disabled: false,
    },
    cancelText: "No",
    onOk() {
      console.log("OK");
    },
    onCancel() {
      console.log("Cancel");
    },
  });
};
const handleUserEdit = () => {
  console.log(handleUserEdit);
};

const onOk = (value) => {
  console.log("onOk: ", value);
};

const itemRender = (_, type, originalElement) => {
  return originalElement;
};

const onPage = (page, pageSize) => {
  console.log(page, pageSize);
};

function Users() {
  // 状态
  let [userEdit, setuserEdit] = useState(false);
  let [userEditRow, setuserEditRow] = useState({});
  const onSearch = (value) => {
    console.log(value);
  };
  // 表格
  const dataSource = [
    {
      key: "1",
      name: "胡彦斌",
      age: 32,
      address: "西湖区湖底公园1号",
      uname: "aaa",
    },
    {
      key: "2",
      name: "胡彦祖",
      age: 42,
      address: "西湖区湖底公园1号",
      uname: "bbb",
    },
    {
      key: "3",
      name: "胡彦祖",
      age: 42,
      address: "西湖区湖底公园1号",
      uname: "ccc",
    },
  ];

  const columns = [
    {
      title: "姓名",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "年龄",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "住址",
      dataIndex: "address",
      key: "address",
    },

    {
      title: "操作",
      key: "operation",
      fixed: "right",
      width: 150,
      render: (text, record, index) => {
        // console.log(text, record, index);
        return (
          <Container>
            <Button
              style={{ marginRight: "10px" }}
              onClick={() => {
                console.log(text, record, index);
                setuserEdit(true);
                setuserEditRow(record);
              }}
            >
              <FormOutlined onClick={handleUserEdit} />
            </Button>
            <Button onClick={handleDel}>
              <DeleteOutlined />
            </Button>
          </Container>
        );
      },
    },
  ];
  // 路由
  const navigate = useNavigate();

  return (
    <Container>
      <UserEdit
        row={userEditRow}
        state={userEdit}
        close={() => setuserEdit(false)}
      />
      <Card
        title="用户列表"
        extra={
          <Button
            type="primary"
            onClick={() => {
              navigate("/admin/users/create");
            }}
          >
            创建
          </Button>
        }
      >
        {/* 筛选 */}
        <div className="filter">
          <Search
            placeholder="请输入用户名"
            onSearch={onSearch}
            enterButton
            style={{ width: "300px" }}
            allowClear
          />
          &nbsp;&nbsp;
          <RangePicker
            showTime={{ format: "HH:mm" }}
            format="YYYY-MM-DD HH:mm"
            onChange={onChange}
            onOk={onOk}
          />
        </div>
        {/* 筛选 */}

        {/* 表格 */}
        <Table dataSource={dataSource} columns={columns} pagination={false} />
        {/* 表格 */}

        {/* 分页 */}
        <Pagination onChange={onPage} total={500} itemRender={itemRender} />
        {/* 分页 */}
      </Card>
    </Container>
  );
}

export default Users;
