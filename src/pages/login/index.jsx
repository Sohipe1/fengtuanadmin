//导入样式
import { Container } from "./styled";
// 导入API
import { useNavigate } from "react-router-dom";
// 导入接口
import { postLoginSmsapi } from "../../api/login";
// 导入组件
import { Form, Select, Input, Button, message } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
const { Option } = Select;

function Login() {
  // 路由
  const navigate = useNavigate();
  // 表单登录
  const onFinish = async (values) => {
    console.log("Success:", values);
    let res = await postLoginSmsapi(values);
    console.log(res);
    message.success("登录成功");
    navigate("/admin");
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
    message.error("登录失败");
  };
  return (
    <Container>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item>
          <h1>锋团本地生活运营平台</h1>
        </Form.Item>
        <Form.Item
          name="question"
          validateTrigger="onBlur"
          rules={[
            {
              required: true,
              message: "请选择密保问题",
            },
          ]}
        >
          <Select placeholder="请选择密保问题">
            <Option value="你大爷的名字">你大爷的名字</Option>
            <Option value="你其中一位讲师的名字">你其中一位讲师的名字</Option>
            <Option value="你叔叔的名字">你叔叔的名字</Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="answer"
          validateTrigger="onBlur"
          rules={[
            {
              required: true,
              message: "请输入密保答案",
            },
          ]}
        >
          <Input placeholder="请输入密保答案" />
        </Form.Item>
        <Form.Item
          name="uname"
          validateTrigger="onBlur"
          rules={[
            {
              validator: (_, value) => {
                if (!value) return Promise.reject(new Error("请输入用户名"));

                if (value.length < 2 || value.length > 10) {
                  return Promise.reject(new Error("用户名只能2~10个字符"));
                }
                return Promise.resolve();
              },
            },
          ]}
        >
          <Input placeholder="请输入用户名" />
        </Form.Item>
        <Form.Item
          name="password"
          validateTrigger="onBlur"
          rules={[
            {
              required: true,
              message: "请输入密码",
            },
          ]}
        >
          <Input.Password
            placeholder="请输入密码"
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            登录
          </Button>
        </Form.Item>
      </Form>
    </Container>
  );
}

export default Login;
