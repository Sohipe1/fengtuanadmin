import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  background-color: #2d3a4b;
  display: flex;
  justify-content: center;
  align-items: center;
  .ant-form {
    width: 300px;
    h1 {
      color: #fff;
      font-size: 30px;
      font-weight: bold;
      margin-bottom: -10px;
    }
    .ant-select .ant-select-selector {
      padding-top: 5px;
      height: 40px;
    }
    .ant-input {
      height: 40px;
    }
    .ant-input-affix-wrapper {
      padding: 0px 5px;
    }
    button {
      width: 100%;
      height: 40px;
    }
  }
`;
